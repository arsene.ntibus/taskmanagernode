const Task = require('../models/Task')
/* Toutes les tâtches*/
const getAllTasks = (req, res) => {
    res.send("Toutes les tâches")
}
/*Create a Task*/
const createTask = async (req, res) => {
    const task = await Task.create(req.body)
    res.status(201).json({task})
}
/* Get  a sigle task*/
const getTask = (req, res) => {
    res.json({id: req.params.id})

}
/*Updtae the tasks*/
function updateTask(req, res) {
    res.send("Mettre à jour une tâche")
}

const deleteTask = (req, res) => {
    res.send("Supprimer une tâche")
}
module.exports = {
    getAllTasks,
    createTask,
    getTask,
    updateTask,
    deleteTask,

}