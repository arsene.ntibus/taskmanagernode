const express = require('express')
const app = express()
const connectDB = require('./db/connect');
const tasks = require('./routes/tasks')
require('dotenv').config()


//middlewares
app.use(express.json())

//Routes
app.use("/api/v1/tasks", tasks)
app.get('/hello', (req, res) => {
    res.send("task Manager App")
})
const PORT = 3301


const starts = async () => {
    try {
        await connectDB(process.env.MONGO_URI)
        app.listen(PORT,
            console.log(`le serveur tourne sur le port ${PORT}`))
    } catch (error) {

    }
}

starts()