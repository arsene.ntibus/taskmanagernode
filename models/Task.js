const mongoose = require('mongoose');
/*La méthode Schemade Mongoose vous permet de créer un schéma de données pour votre base de données MongoDB.*/
const TaskSchema = new mongoose.Schema({
    /* Validaturs:  */
    name: {
        type: String,
        required: [true, "Doit contenir le name"],
        trim: true,
        maxlength: [24, "le name ne peux pas dépassé 24 characters"],
    },
    completed: {
        type: Boolean,
        default:false,
    },
})
module.exports = mongoose.model('Task', TaskSchema);
/*La méthode  model  transforme ce modèle en un modèle utilisable.*/
